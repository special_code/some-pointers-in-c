#include<stdio.h>

void  change_addres(char *a , char *b){
      char temp;
      temp = *a;

     (*a)= (*b);
     (*b) =temp;

}

int main(void){

     char end1= 'A';
     char end2= 'B';
     char end3= 'C';
     char end4= 'D';
     char end5= 'E';
     char end6= 'F';
     char *ptr= NULL;

    ptr = &end6;

    printf("ANTES DA FUNCAO SER CHAMADA ENDERECOS DAS VARIAVEIS\n");
    printf("==================== MEMORY =====================\n");
    printf("NOME VARIAVEL | VALOR | ENDERECO HEXADECIMAL| ENDERECO DECIMAL\n");
    printf("--------------------------------------------------------------\n");
    printf("CHAR1         | %c    |     %p   | %d\n", end1 , &end1, &end1);
    printf("CHAR2         | %c    |     %p   | %d\n", end2 , &end2, &end2);
    printf("CHAR3         | %c    |     %p   | %d\n", end3 , &end3, &end3);
    printf("CHAR4         | %c    |     %p   | %d\n", end4 , &end4, &end4);
    printf("CHAR5         | %c    |     %p   | %d\n", end5 , &end5, &end5);
    printf("CHAR6         | %c    |     %p   | %d\n", end6 , &end6, &end6);

    printf("\nMUDANDANDO OS ENDERECOS CHAMANDO A FUNCAO\n");

    change_addres(&end1 , &end2);
    printf("CHAR1         | %c    |     %p   | %d\n",end1, &end1 , &end1);
    printf("CHAR2         | %c    |     %p   | %d\n", end2 , &end2, &end2);
    change_addres(&end3 , &end4);
    printf("CHAR3         | %c    |     %p   | %d\n", end3 , &end3, &end3);
    printf("CHAR4         | %c    |     %p   | %d\n", end4 , &end4, &end4);
    change_addres(&end5 ,&end6);
    printf("CHAR5         | %c    |     %p   | %d\n", end5 , &end5, &end5);
    change_addres(ptr, &end6);
    printf("CHAR6         | %c    |     %p   | %d\n",*ptr , &ptr, &ptr);

  return 0;
}

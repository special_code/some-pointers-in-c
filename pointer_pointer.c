#include<stdio.h>
#include<stdlib.h>

int main(void){
  int *ptr1;
  int **ptr2;

  int valor = 10;

  ptr1 = &valor;
  ptr2 = &ptr1;

  printf("ENDERECO DE PTR 1= %d\n", ptr1);
  printf("ENDERECO DE PTR 2= %d\n", ptr2);

  printf("\nVALOR DE PTR 1= %d\n", *ptr1);
  printf("VALOR DE PTR 2 = %d\n", *ptr2);



  return EXIT_SUCCESS;
}

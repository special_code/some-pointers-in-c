#include<stdio.h>
/*passagem por referencia passando o endereco para funcao nao a copia da variavel*/
void dobro(int *num){
   (*num)*=2;
}

int main(void){
    int num;
    printf("Digite um numero: ");
    scanf("%d", &num);

    dobro(&num);
    printf("O dobro dele eh: %d\n", num);


 return 0;
}

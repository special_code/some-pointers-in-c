#include<stdio.h>

int main(void){
   /*DECLARANDO DUAS VARIAVEIS DO TIPO INT E DUAS DO TIPO CHAR TODAS INICIALIZADAS*/
   int num1= 1;
   int num2= 2;
   /*VARIAVEIS CHARS*/
   char letraA=0x42;
   char letraB=0x43;
    /*EXEBINDO O VALOR DAS VARIAVEIS E SEUS ENDERECOS*/
   printf("\nVARIAVEL num1 tem seu valor: %d seu endereco: %d\n", num1 , &num1 );
   printf("VARIAVEL num2 tem seu valor: %d seu endereco: %d\n", num2 , &num2);
   printf("\n");
   printf("VARIAVEL letraA tem seu valor: %c seu endereco: %d\n", letraA , &letraA);
   printf("VARIAVEL letraB tem seu valor: %c seu endereco: %d\n", letraB , &letraB);

   printf("\n==========+ MUDANDO OS VALORES DAS VARIAVEIS -==========\n");
   /*mudando seu valores*/
   num1= 2112;
   num2= 666;
   /*mudando seus valores*/
   letraA= 'A';
   letraB= 'B';
   /*exibindo seus novos valores e enderecos */
   printf("\nVARIAVEL num1 tem seu valor: %d seu endereco: %d\n", num1 , &num1 );
   printf("VARIAVEL num2 tem seu valor: %d seu endereco: %d\n", num2 , &num2);
   printf("\n");
   printf("VARIAVEL letraA tem seu valor: %c seu endereco: %d\n", letraA , &letraA);
   printf("VARIAVEL letraB tem seu valor: %c seu endereco: %d\n", letraB , &letraB);
   printf("\n");

  return 0;
}

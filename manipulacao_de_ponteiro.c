#include<stdio.h>

/* 
 *
 * author: Felipe
 *
 *  
*/


 /*==========+ MUDADANDO AS VARIAVEIS DE ENDERECOS -==========*/
int main(void){

    /*DUAS VARIAVEIS DO TIPO CHAR QUE SERAO MANIPULADAS*/
     char char_one= 'A';
     char char_two= 'B';
  
    /*DECLARANDO TRES PONTEIROS*/
     char *ptr1 = NULL;
     char *ptr2 = NULL; 
     char *ptr3=NULL;
  
    /*IMPRIMINDO OS VALORES DOIS CHAR*/
    printf("CHAR_ONE TEM SEU VALOR ATRIBUIDO: %c\n", char_one);
    printf("SEU ENDERECO: %d\n" , &char_one);
    printf("\nCHAR_TWO TEM SEU VALOR ATRIBUIDO: %c\n", char_two);
    printf("SEU ENDERECO: %d\n", &char_two);
    
    /*ENDERECOS DOS DOIS PONTEIROS*/
    printf("\n====================-ENDERECOS DOS DOIS PONTEIROS+===================\n");
    printf("\nPONTEIRO 1 SEU ENDERECO: %d\n", ptr1);
    printf("PONTEIRO 2 SEU ENDERECO: %d\n", ptr2);
    
    /*IFORMANDO O USARIO QUE OS PONTEIRO RECEBERAM OS VALORES DAS DUAS VARIAVIES*/
    printf("\n===================+PONTEIROS RECEBENDO VALORES E O ENDERECO DE CHAR_ONE E CHAR_TWO-====================\n");
    ptr1 = &char_one; //ptr1 recebe o valor e o endereco de char_one
    printf("\nENDERECO DO PONTEIRO 1 E: %d\n" , ptr1);
    printf("VALOR ATRIBUIDO: %c\n", *ptr1);

    ptr2 = &char_two;//ptr2 recbendo o valor e o endereco de char_two
    printf("\nENDERECO DO PONTEIRO 2 E: %d\n" , ptr2);
    printf("VALOR ATRIBUIDO: %c\n", *ptr2);
    
    /*MANIPULANDO OS ENDERECO DE CHAR_ONE E CHAR_TWO*/
    printf("\n===================+CHAR_ONE RECEBE O ENDRECO DE CHAR_TWO E CHAR_TWO RECEBE DE CHAR_ONE-====================\n");
    ptr3 = ptr1;//ptr3 esta vazio e recebe o valor e o enderco de char_one antes de ser manipulada abaixo
    ptr1 = ptr2;
    printf("\nEndereco PONTEIRO 1: %d\n", ptr1);
    printf("SEU VALOR: %c\n", *ptr1);

    ptr2 = ptr3;
    printf("\nENDERECO DE PONTEIRO 2: %d\n", ptr2);
    printf("SEU VALOR: %c\n" , *ptr2);

 return 0;
}
